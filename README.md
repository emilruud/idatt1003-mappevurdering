# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).
 
 Canidate number ID = "10005"

## Project description

This project is an assessment in the subject IDATT1003 "programmering 1". In the project it is developed a simplified system for handling train departures from a single train station.

## Project structure

This project has its nontesting files in src/main/java/edu.ntnu.stud folder, and its testing classes in test/java/edu.ntnu.stud

## Link to repository

https://gitlab.stud.idi.ntnu.no/emilruud/idatt1003-mappevurdering

## How to run the project

To run the project you have to run the file called TrainDispatchApp.java which is the main class of the programm, and inclued the main method. When the project is run it should print out a menu with diffrent option, where the user can write thier choice in the console.

## How to run the tests

To run the test you need to run the two test files TrainScheduleTest and TrainDeparture, located in test/java/edu.ntnu.stud.
