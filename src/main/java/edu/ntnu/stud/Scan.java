package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.Scanner;

/**
 * This class provides methods for scanning user input.
 */
public class Scan {
  private static final Scanner sc = new Scanner(System.in);

  private Scan() {}

  /**
   * Scans the user input for a string.
   *
   * @param parameterName The name of the parameter to scan.
   * @return The string the user entered.
   */
  public static String scanString(String parameterName) {
    String out = "";
    while (out.isEmpty()) {
      System.out.print("Enter " + parameterName + ": ");
      if (sc.hasNextLine()) {
        out = sc.nextLine();
      }
      if (out.isEmpty()) {
        System.out.println("Please enter a valid string");
      }
    }
    return out;
  }

  /**
   * Scans the user input for a train number.
   *
   * @return The train number the user entered.
   */
  public static String scanTrainNumber() {
    String out = "";
    while (out.isEmpty()) {
      System.out.print("Enter train number: ");
      String userInput = sc.nextLine();

      if (isStringAnInteger(userInput)) {
        out = userInput;
      } else {
        if (!userInput.isEmpty()) {
          System.out.println("Train number must be a number");
        } else {
          System.out.println("Train number cannot be empty");
        }
      }
    }
    return out;
  }

  /**
   * Scans the user input for a destination.
   *
   * @return The destination the user entered.
   */
  private static boolean isStringAnInteger(String str) {
    try {
      Integer.parseInt(str);
      // If no exception is thrown, the string is a valid integer
      return true;
    } catch (NumberFormatException e) {
      // The string is not a valid integer
      return false;
    }
  }

  /**
   * Scans the user input for a track.
   *
   * @return The track the user entered.
   */
  public static int scanTrack() {
    int out = 0;
    while (out == 0 || out < -1) {
      System.out.print("Enter track: ");

      if (sc.hasNextInt()) {
        out = sc.nextInt();
        sc.nextLine();

        if (out == 0 || out < -1) {
          System.out.println("Track must be a positive number or -1 if no track is assigned");
        }
      } else {
        System.out.println("Invalid input. Please enter a valid integer for the track.");
        sc.next(); // Consume invalid input to avoid an infinite loop
      }
    }
    return out;
  }

  /**
   * Scans the user input for a time.
   *
   * @param parameterName The name of the parameter to scan.
   * @return The time the user entered.
   */
  public static LocalTime scanTime(String parameterName) {
    LocalTime out = null;
    while (out == null) {
      System.out.print("Enter " + parameterName + " (hh:mm): ");
      if (sc.hasNextLine()) {
        String timeString = sc.nextLine();
        try {
          out = LocalTime.parse(timeString);
        } catch (Exception e) {
          System.out.println("Please enter a valid time (hh:mm)");
        }
      }
    }
    return out;
  }

  /**
   * Checks if the user wants to assign a track to the train departure.
   *
   * @return True if the user wants to assign a track, false otherwise.
   */
  public static boolean isTrackAssigned() {
    boolean validResponse = false;
    boolean isTrackAssigned = false;

    while (!validResponse) {
      System.out.print("Do you want to assign track? (y/n): ");
      String userInput = sc.nextLine();

      if (userInput.trim().equalsIgnoreCase("y")) {
        isTrackAssigned = true;
        validResponse = true;
      } else if (userInput.trim().equalsIgnoreCase("n")) {
        validResponse = true;
      } else {
        System.out.println("Invalid response. Please enter 'y' or 'n'.");
      }
    }

    return isTrackAssigned;
  }
}

