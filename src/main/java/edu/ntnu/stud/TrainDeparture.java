package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * <p>This class represents a train departure.</p>
 *
 * <p>Each TrainDeparture object has the following properties:</p>
 * <ul>
 *   <li>departureTime: The departure time of the train.</li>
 *   <li>line: The line the train is on.</li>
 *   <li>trainNumber: The train number.</li>
 *   <li>destination: The destination the train is heading to.</li>
 *   <li>track: The track the train is on.</li>
 *   <li>delay: Any delay the train might have.</li>
 * </ul>
 */
public class TrainDeparture {
  private final LocalTime departureTime;
  private final String line;
  private final String trainNumber;
  private final String destination;
  private int track;
  private LocalTime delay;

  /**
     * Creates a new TrainDeparture object with the specified parameters.
     *
     * @param departureTime The departure time.
     * @param line          The line.
     * @param trainNumber   The train number.
     * @param destination   The destination.
     * @param track         The track.
     * @param delay         The delay.
     */
  public TrainDeparture(LocalTime departureTime, String line, String trainNumber,
                        String destination, int track, LocalTime delay) {
    verifyTime(departureTime, "Departure Time");
    verifyStringParameter(line, "Line");
    verifyStringParameter(trainNumber, "Train Number");
    verifyStringParameter(destination, "Destination");

    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    setTrack(track);
    setDelay(delay);
  }

  /**
     * Creates a new TrainDeparture object with the specified parameters.
     * This constructor sets the delay to zero by default.
     *
     * @param departureTime The departure time.
     * @param line          The line.
     * @param trainNumber   The train number.
     * @param destination   The destination.
     * @param track         The track.
     */
  public TrainDeparture(LocalTime departureTime, String line, String trainNumber,
                        String destination, int track) {
    this(departureTime, line, trainNumber, destination, track, LocalTime.of(0, 0));
  }

  private static void verifyStringParameter(String parameter, String paramName) {
    if (parameter == null || parameter.trim().isEmpty()) {
      throw new IllegalArgumentException(paramName + " cannot be null or empty.");
    }
  }

  private static void verifyTrack(int track) {
    if (track == 0 || track < -1) {
      throw new IllegalArgumentException("Invalid track: " + track + ". Track must be a positive"
        + " number or -1 if no track is assigned");
    }
  }

  private  void verifyTime(LocalTime time, String paramName) {
    if (time == null) {
      throw new IllegalArgumentException(paramName + "cannot be null.");
    }
  }

  public LocalTime getDepartureTime() {
    return departureTime;
  }

  public String getLine() {
    return line;
  }

  public String getTrainNumber() {
    return trainNumber;
  }

  public String getDestination() {
    return destination;
  }

  public int getTrack() {
    return track;
  }

  public LocalTime getDelay() {
    return delay;
  }

  /**
   * Sets the track for this train departure.
   * <p>
   * This method sets the track for this train departure to the specified track.
   * The track must be a positive number or -1 if no track is assigned.
   * If the track is not a positive number or -1, an {@link IllegalArgumentException}
   * will be thrown.
   * </p>
   *
   * @param track The track to set. Must be a positive number or -1 if no track is assigned.
   * @throws IllegalArgumentException If the track is not a positive number or -1.
   */
  public void setTrack(int track) {
    verifyTrack(track);
    this.track = track;
  }

  /**
   * <p>Sets the delay for this train departure.</p>
   *
   * <p>This method sets the delay for this train departure to the specified delay.
   * The delay must be a {@link LocalTime} object representing a time between 00:00 and 23:59.
   * If the delay is not within this range, an {@link IllegalArgumentException} will be thrown.</p>
   *
   * <p>Example usage:</p>
   * <code>
   * TrainDeparture trainDeparture = new TrainDeparture(departureTime,
   * line, trainNumber, destination, track);
   * trainDeparture.setDelay(LocalTime.of(1, 0));
   * </code>
   *
   * @param delay The delay to set. Must be a {@link LocalTime} object representing
   *        a time between 00:00 and 23:59.
   * @throws IllegalArgumentException If the delay is not a {@link LocalTime} object
   *        representing a time between 00:00 and 23:59.
   */
  public void setDelay(LocalTime delay) {
    verifyTime(delay, "Delay");

    int maxHours = 23 - departureTime.getHour();
    int maxMinutes = 59 - departureTime.getMinute();

    if (delay.getHour() > maxHours
        || (delay.getHour() == maxHours && delay.getMinute() > maxMinutes)) {
      throw new IllegalArgumentException("Delay must be inside current day");
    }

    this.delay = delay;
  }

  /**
   * <p>Gets the departure time with the delay.</p>
   *
   * <p>This method returns the departure time with the delay.
   * If the delay is zero, the departure time will be returned.</p>
   *
   * <p>Example usage:</p>
   * <code>
   * TrainDeparture trainDeparture = new TrainDeparture(departureTime,
   * line, trainNumber, destination, track);
   * trainDeparture.setDelay(LocalTime.of(1, 0));
   * trainDeparture.getDepartureTimeWithDelay(); // Returns departureTime + delay
   * </code>
   *
   * @return The departure time with the delay.
   */
  public LocalTime getDepartureTimeWithDelay() {
    return departureTime.plusHours(delay.getHour()).plusMinutes(delay.getMinute());
  }

  @Override
  public String toString() {
    String delayString = delay.equals(LocalTime.of(0, 0)) ? "" : delay
        + " Expected departure time: " + getDepartureTimeWithDelay().toString();
    String trackString = (track != -1) ? String.valueOf(track) : ""; // Check if track is not -1

    return String.format("| %-15s| %-15s| %-15s| %-27s| %-40s| %-5s |", departureTime, line,
      trainNumber, destination, delayString, trackString);
  }
}