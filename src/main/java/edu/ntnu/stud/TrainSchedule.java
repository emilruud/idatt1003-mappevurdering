package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * This class represents a train schedule.
 */
public class TrainSchedule {
  private final ArrayList<TrainDeparture> trainDepartures;
  private LocalTime time;

  public TrainSchedule(LocalTime time) {
    trainDepartures = new ArrayList<>();
    this.time = time;
  }

  public LocalTime getTime() {
    return time;
  }

  /**
   * Sets the time to the specified time.
   *
   * @param newTimeString The new time.
   */
  public void setTime(String newTimeString) {
    LocalTime newTime = LocalTime.parse(newTimeString);
    if (newTime.isBefore(this.time)) {
      throw new IllegalArgumentException("Cant set time to earlier than current time");
    }
    this.time = newTime;
    removeTrainDepaturesEarlierThanTime();
  }

  /**
   * Adds a new TrainDeparture object to the list of train departures.
   *
   * @param trainDeparture The TrainDeparture object to add.
   */
  public void addTrainDeparture(TrainDeparture trainDeparture) {
    boolean trainNumberExists = trainDepartures.stream()
        .anyMatch(existingDeparture -> existingDeparture.getTrainNumber()
          .equals(trainDeparture.getTrainNumber()));

    if (trainNumberExists) {
      throw new IllegalArgumentException("Train number already exists");
    }

    trainDepartures.add(trainDeparture);
  }

  /**
   * Finds and returns a TrainDeparture object based on the specified train number.
   *
   * @param trainNumber The train number to search for.
   * @return The TrainDeparture object matching the specified train number.
   */
  public TrainDeparture findTrainDepartureByNumber(String trainNumber) {
    return trainDepartures.stream()
      .filter(trainDeparture -> trainDeparture.getTrainNumber().equals(trainNumber))
      .findFirst()
      .orElse(null);
  }

  /**
   * Finds and returns a list of TrainDeparture objects based on the specified destination.
   *
   * @param destination The destination to search for.
   * @return A list of TrainDeparture objects matching the specified destination.
   */
  public List<TrainDeparture> findTrainDepartureByDestination(String destination) {
    if (destination == "") {
      throw new IllegalArgumentException("Destination cannot be empty");
    }

    List<TrainDeparture> result = new ArrayList<>(trainDepartures.stream()
        .filter(trainDeparture -> trainDeparture.getDestination().equalsIgnoreCase(destination))
        .toList());

    //Sort the list of train departures based on departure time
    result.sort(Comparator.comparing(TrainDeparture::getDepartureTime));
    return result;
  }

  /**
   * Removes all train departures earlier than the current time.
   */
  public void removeTrainDepaturesEarlierThanTime() {
    trainDepartures.removeIf(trainDeparture -> trainDeparture.getDepartureTimeWithDelay()
        .isBefore(this.time));
  }

  /**
   * return a list of all train departures, sorted by departure time.
   */
  public ArrayList<TrainDeparture> sortTrainSchedule() {
    ArrayList<TrainDeparture> result = new ArrayList<>(trainDepartures);
    // Sort the train departures based on departure time
    result.sort(Comparator.comparing(TrainDeparture::getDepartureTime));
    return result;
  }

  /**
   * Removes a train departure from the list of train departures.
   *
   * @param trainNumber the trainNumber to remove.
   */
  public void removeExistingTrainDepartureByNumber(String trainNumber) {
    TrainDeparture trainDeparture = findTrainDepartureByNumber(trainNumber);
    if (trainDeparture != null) {
      trainDepartures.remove(trainDeparture);
    }
  }
}
