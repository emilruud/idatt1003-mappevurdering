package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the user interface for the train dispatch application.
 */
//@SuppressWarnings("checkstyle:Indentation")
public class UserInterface {
  private static final String ADD_TRAINDEPARTURE = "1";
  private static final String  SHOW_INFORMATION_BOARD = "2";
  private static final String  SET_TIME = "3";
  private static final String FIND_TRAINDEPARTURE_BY_NUMBER = "4";
  private static final String  FIND_TRAINDEPARTURE_BY_DESTINATION = "5";
  private static final String  SET_DELAY = "6";
  private static final String  SET_TRACK = "7";
  private static final String  REMOVE_TRAINDEPARTURE_BY_NUMBER = "8";
  private static final String  EXIT = "9";

  private TrainSchedule trainSchedule;


  /**
   * Initializes the application.
   */
  public void init() {

    trainSchedule = new TrainSchedule(LocalTime.of(10, 0));
    TrainDeparture tog1 = new TrainDeparture(LocalTime.of(13, 16), "L1", "1", "Spikkestad", 5);
    trainSchedule.addTrainDeparture(tog1);
    TrainDeparture tog2 = new TrainDeparture(LocalTime.of(13, 18), "L1", "2",
        "Lillestrøm", 6, LocalTime.of(0, 10));
    trainSchedule.addTrainDeparture(tog2);
    TrainDeparture tog3 = new TrainDeparture(LocalTime.of(13, 19), "L13", "3",
        "Dal", 3, LocalTime.of(0, 10));
    trainSchedule.addTrainDeparture(tog3);
    TrainDeparture tog4 = new TrainDeparture(LocalTime.of(13, 20), "L2", "4", "Dal", 4);
    trainSchedule.addTrainDeparture(tog4);
    TrainDeparture tog5 = new TrainDeparture(LocalTime.of(13, 21),
        "L3", "5", "Trondheim Sentralstasjon", 5);
    trainSchedule.addTrainDeparture(tog5);
  }

  /**
   * Shows the menu and returns the user's choice.
   *
   * @return The user's choice.
   */
  public String showMenu() {
    System.out.println("1. Add train departure");
    System.out.println("2. Show information board");
    System.out.println("3. Set time");
    System.out.println("4. Find train departure by number");
    System.out.println("5. Find train departure by destination");
    System.out.println("6. Set delay");
    System.out.println("7. Set Track");
    System.out.println("8. Remove train departure by number");
    System.out.println("9. Exit");

    return Scan.scanString("choice");
  }

  /**
   * Starts the application.
   */
  public void start() {
    boolean running = true;
    while (running) {
      System.out.println();
      String choice = showMenu();
      switch (choice) {
        case ADD_TRAINDEPARTURE -> addTrainDeparture();
        case SHOW_INFORMATION_BOARD -> showInformationBoard();
        case SET_TIME -> setTime();
        case FIND_TRAINDEPARTURE_BY_NUMBER -> findTrainDepartureByNumber();
        case FIND_TRAINDEPARTURE_BY_DESTINATION -> findTrainDepartureByDestination();
        case SET_DELAY -> setDelay();
        case SET_TRACK -> setTrack();
        case REMOVE_TRAINDEPARTURE_BY_NUMBER -> removeTrainDepartureByNumber();
        case EXIT -> running = false;
        default -> System.out.println("Invalid choice");
      }
    }
    System.out.println("Thank you for using the train dispatch application.");
  }

  /**
   * Adds a new train departure.
   */
  public void addTrainDeparture() {
    LocalTime departureTime = Scan.scanTime("Departure time");
    String line = Scan.scanString("Line");
    String trainNumber = Scan.scanTrainNumber();
    String destination = Scan.scanString("Destination");
    int track = -1;
    if (Scan.isTrackAssigned())  {
      track = Scan.scanTrack();
    }
    try {
      TrainDeparture trainDeparture = new TrainDeparture(departureTime,
            line, trainNumber, destination, track);
      trainSchedule.addTrainDeparture(trainDeparture);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Shows the information board.
   */
  private void showInformationBoard() {

    printInformationBoardOverview();

    // Remove train departures earlier than the current time
    trainSchedule.removeTrainDepaturesEarlierThanTime();

    // Get the updated train departures after removal
    ArrayList<TrainDeparture> updatedDepartures = trainSchedule.sortTrainSchedule();

    // Print the updated train departures
    printTrainDepatures(updatedDepartures);

    printBottomLine();
  }


  private void printBottomLine() {
    System.out.println("-".repeat(131));
  }

  /**
   * Prints the information board overview.
   */
  private void printInformationBoardOverview() {
    // Print a line of hyphens above the "Current Time" box
    System.out.println("-".repeat(131));

    // Print the "Current Time" box with the current time inside
    System.out.println("|" + " ".repeat(55) + "Current Time " + trainSchedule.getTime()
        + " ".repeat(56) + "|");

    // Print a line of hyphens below the "Current Time" box
    System.out.println("-".repeat(131));

    //System.out.println();
    System.out.printf(("| %-15s| %-15s| %-15s| %-27s| %-40s| %-5s |"), "Departure Time",
        "Line", "Train number", "Destination", "Delay", "Track");
    System.out.println();
    System.out.println("-".repeat(131)); // Underline
  }

  /**
   * Prints the specified train departures.
   *
   * @param trainDepartures The train departures to print.
   */
  private void printTrainDepatures(List<TrainDeparture> trainDepartures) {
    for (TrainDeparture trainDeparture : trainDepartures) {
      System.out.println(trainDeparture);
    }
  }

  /**
   * * Sets the time of day.
   */
  private void setTime() {
    String newTimeString = Scan.scanTime("new time").toString();
    try {
      trainSchedule.setTime(newTimeString);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Finds a train departure by number.
   */
  private void findTrainDepartureByNumber() {
    String trainNumber = Scan.scanTrainNumber();
    TrainDeparture trainDeparture = trainSchedule
        .findTrainDepartureByNumber(trainNumber);
    if (trainDeparture != null) {
      printInformationBoardOverview();
      System.out.println(trainDeparture);
      printBottomLine();
    } else {
      System.out.println("Train number not found");
    }
  }

  /**
   * Finds a train departure by destination.
   */
  private void findTrainDepartureByDestination() {
    String destination = Scan.scanString("Destination");
    List<TrainDeparture> trainDepartures = trainSchedule
        .findTrainDepartureByDestination(destination);
    if (trainDepartures != null) {
      printInformationBoardOverview();
      printTrainDepatures(trainDepartures);
      printBottomLine();
    } else {
      System.out.println("Destination not found");
    }
  }

  /**
   * Sets the delay of a train departure.
   */
  private void setDelay() {
    String trainNumber = Scan.scanTrainNumber();
    TrainDeparture trainDeparture = trainSchedule.findTrainDepartureByNumber(trainNumber);

    if (trainDeparture != null) {
      try {
        LocalTime delay = Scan.scanTime("delay");
        trainDeparture.setDelay(delay);
      } catch (IllegalArgumentException e) {
        System.out.println(e.getMessage());
      }
    } else {
      System.out.println("Train number not found");
    }
  }

  /**
   * Sets the track of a train departure.
   */
  private void setTrack() {
    String trainNumber = Scan.scanTrainNumber();
    TrainDeparture trainDeparture = trainSchedule
        .findTrainDepartureByNumber(trainNumber);
    if (trainDeparture != null) {
      int trackNumber = Scan.scanTrack();
      trainDeparture.setTrack(trackNumber);
    } else {
      System.out.println("Train number not found");
    }
  }

  /**
   * Removes a train departure by number.
   */
  private void removeTrainDepartureByNumber() {
    String trainNumber = Scan.scanTrainNumber();
    if (trainSchedule.findTrainDepartureByNumber(trainNumber) == null) {
      System.out.println("Train number not found");
    } else {
      trainSchedule.removeExistingTrainDepartureByNumber(trainNumber);
      System.out.println("Train departure removed");
    }
  }

}
