package edu.ntnu.stud;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TrainDepartureTest {

  private TrainDeparture trainDeparture;
  private LocalTime departureTime;
  private String line;
  private String trainNumber;
  private String destination;
  private int track;
  private LocalTime delay;

  @BeforeEach
  @DisplayName("Set up")
  public void setUp() {
    departureTime = LocalTime.of(10, 0);
    line = "L2";
    trainNumber = "1";
    destination = "Oslo";
    track = 2;
    delay = LocalTime.of(0, 0);

    trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, track, delay);
  }

  @Test
  @DisplayName("Positive test")
  public void testValidTrainDeparture() {

    // Test at henting av informasjon gir forventede verdier
    assertEquals(departureTime, trainDeparture.getDepartureTime());
    assertEquals(line, trainDeparture.getLine());
    assertEquals(trainNumber, trainDeparture.getTrainNumber());
    assertEquals(destination, trainDeparture.getDestination());
    assertEquals(track, trainDeparture.getTrack());
    assertEquals(LocalTime.of(0, 0), trainDeparture.getDelay()); // Sjekk at forsinkelse er initialisert riktig
  }



  @Test
  @DisplayName("Positive test")
  public void testSetDelay() {
    // Test at settDelay fungerer som forventet
    LocalTime delay = LocalTime.of(0, 15);
    trainDeparture.setDelay(delay);
    assertEquals(delay, trainDeparture.getDelay());
  }

  @Test
  @DisplayName("Negative test")
  public void testInvalidSetDelay() {
    assertThrows(IllegalArgumentException.class, () -> trainDeparture.setDelay(LocalTime.of(15, 0)));
  }

  @Test
  @DisplayName("Negative test")
  public void testInvalidTrainDeparture() {
    int track = 0;

    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(departureTime, line, trainNumber, destination, track));
  }

  @Test
  public void testGetDepartureTimeWithDelay() {
    // Arrange
    LocalTime departureTime = LocalTime.of(10, 0);
    LocalTime delay = LocalTime.of(1, 0);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, track, delay);
    // Act
    LocalTime result = trainDeparture.getDepartureTimeWithDelay();

    // Assert
    assertEquals(LocalTime.of(11, 0), result);
  }

  @Test
  @DisplayName("Negative test")
  public void testInvalidTimeTrainDeparture() {

    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(null, line, trainNumber, destination, track));
  }

  @Test
  @DisplayName("Negative test")
  public void testInvalidStringTrainDeparture() {

    assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(departureTime, "", trainNumber, destination, track));
  }

  @Test
  public void testToStringNoDelayNoTrack() {
    String expected = "| 10:00          | L2             | 1              | Oslo                       |                                         | 2     |";
    assertEquals(expected, trainDeparture.toString());
  }

  @Test
  public void testToStringWithDelayAndTrack() {
    trainDeparture.setDelay(LocalTime.of(1, 0));
    trainDeparture.setTrack(-1);

    String expected = "| 10:00          | L2             | 1              | Oslo                       | 01:00 Expected departure time: 11:00    |       |";
    assertEquals(expected, trainDeparture.toString());
  }
}
