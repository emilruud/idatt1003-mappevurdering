package edu.ntnu.stud;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;


import static org.junit.jupiter.api.Assertions.*;
public class TrainScheduleTest {
  private TrainSchedule trainSchedule;
  @BeforeEach
  @DisplayName("Set up")
  void setUp() {
    // Arrange
    LocalTime time = LocalTime.of(10, 0);
    trainSchedule = new TrainSchedule(time);
  }

  @Test
  @DisplayName("Positive test")
  void setTime_validTime_shouldSetTime() {
    // Arrange
    String newTimeString = "10:15";
    LocalTime expectedTime = LocalTime.parse(newTimeString);

    // Act
    trainSchedule.setTime(newTimeString);

    // Assert
    assertEquals(expectedTime, trainSchedule.getTime());
  }

  @Test
  @DisplayName("Negative test")
  void setTime_invalidTime_shouldThrowException() {
    // Arrange
    TrainSchedule trainSchedule = new TrainSchedule(LocalTime.of(10, 0));
    String newTimeString = "09:15";

    // Act & Assert
    assertThrows(IllegalArgumentException.class, () -> trainSchedule.setTime(newTimeString));
  }

  @Test
  @DisplayName("Positive test")
  void addTrainDeparture_validTrainDeparture_shouldAddTrainDeparture() {
    // Arrange
    TrainSchedule trainSchedule = new TrainSchedule(LocalTime.of(10, 0));
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(10, 0), "L2", "1", "Oslo", 2);

    // Act
    trainSchedule.addTrainDeparture(trainDeparture);

    // Assert
    assertEquals(trainDeparture, trainSchedule.findTrainDepartureByNumber("1"));
  }

  @Test
  @DisplayName("Negative test")
  void addTrainDeparture_duplicateTrainNumber_shouldThrowException() {
    // Arrange
    TrainSchedule trainSchedule = new TrainSchedule(LocalTime.of(10, 0));
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(10, 0), "L2", "1", "Oslo", 2);
    trainSchedule.addTrainDeparture(trainDeparture);

    // Act & Assert
    assertThrows(IllegalArgumentException.class, () -> trainSchedule.addTrainDeparture(trainDeparture));
  }

  @Test
  @DisplayName("Positive test")
  void findTrainDepartureByNumber_validTrainNumber_shouldReturnTrainDeparture() {
    // Arrange
    TrainSchedule trainSchedule = new TrainSchedule(LocalTime.of(10, 0));
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(10, 0), "L2", "1", "Oslo", 2);
    trainSchedule.addTrainDeparture(trainDeparture);

    // Act
    TrainDeparture result = trainSchedule.findTrainDepartureByNumber("1");

    // Assert
    assertEquals(trainDeparture, result);
  }

  @Test
  @DisplayName("Negative test")
  void findTrainDepartureByNumber_invalidTrainNumber_shouldReturnNull() {
    // Arrange
    TrainSchedule trainSchedule = new TrainSchedule(LocalTime.of(10, 0));
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(10, 0), "L2", "1", "Oslo", 2);
    trainSchedule.addTrainDeparture(trainDeparture);

    // Act
    TrainDeparture result = trainSchedule.findTrainDepartureByNumber("2");

    // Assert
    assertNull(result);
  }

  @Test
  @DisplayName("Positive test")
  void findTrainDepartureByDestination_validDestination_shouldReturnTrainDeparture() {
    // Arrange
    TrainSchedule trainSchedule = new TrainSchedule(LocalTime.of(10, 0));
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(10, 0), "L2", "1", "Oslo", 2);
    trainSchedule.addTrainDeparture(trainDeparture);

    // Act
    TrainDeparture result = trainSchedule.findTrainDepartureByDestination("Oslo").get(0);

    // Assert
    assertEquals(trainDeparture, result);
  }

  @Test
  @DisplayName("Negative test")
  void findTrainDepartureByDestination_invalidDestination_shouldReturnEmptyList() {
    // Arrange
    TrainSchedule trainSchedule = new TrainSchedule(LocalTime.of(10, 0));
    TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(10, 0), "L2", "1", "Oslo", 2);
    trainSchedule.addTrainDeparture(trainDeparture);

    // Act
    List<TrainDeparture> result = trainSchedule.findTrainDepartureByDestination("Trondheim");

    // Assert
    assertTrue(result.isEmpty());
  }

  @Test
  @DisplayName("Positive test")
  public void testFindTrainDeparturesByDestination() {
    trainSchedule.addTrainDeparture(new TrainDeparture(LocalTime.of(10, 0), "L2", "1", "Oslo", 2));
    trainSchedule.addTrainDeparture(new TrainDeparture(LocalTime.of(10, 0), "L2", "2", "Oslo", 2));

    List<TrainDeparture> result = trainSchedule.findTrainDepartureByDestination("Oslo");
    assertEquals(2, result.size());
  }

  @Test
  @DisplayName("Negative test")
  public void testFindTrainDeparturesByDestinationNoMatch() {
    trainSchedule.addTrainDeparture(new TrainDeparture(LocalTime.of(10, 0), "L2", "1", "Oslo", 2));
    trainSchedule.addTrainDeparture(new TrainDeparture(LocalTime.of(10, 0), "L2", "2", "Oslo", 2));

    List<TrainDeparture> result = trainSchedule.findTrainDepartureByDestination("Trondheim");
    assertTrue(result.isEmpty());
  }

  @Test
  @DisplayName("Negative test")
  public void testInvalidSearchTrainDeparturesByDestination() {
    assertThrows(IllegalArgumentException.class, () -> trainSchedule.findTrainDepartureByDestination(""));
  }

  @Test
  @DisplayName("Positive test")
  void testSortTrainSchedule() {
    // Arrange
    TrainDeparture trainDeparture1 = new TrainDeparture(LocalTime.of(10, 0), "L2", "1", "Oslo", 2);
    TrainDeparture trainDeparture2 = new TrainDeparture(LocalTime.of(11, 0), "L2", "2", "Oslo", 2);
    TrainDeparture trainDeparture3 = new TrainDeparture(LocalTime.of(9, 0), "L2", "3", "Oslo", 2);

    trainSchedule.addTrainDeparture(trainDeparture1);
    trainSchedule.addTrainDeparture(trainDeparture2);
    trainSchedule.addTrainDeparture(trainDeparture3);

    // Act
    ArrayList<TrainDeparture> sortedTrainDepartures = trainSchedule.sortTrainSchedule();

    // Assert
    assertEquals(trainDeparture3, sortedTrainDepartures.get(0));
    assertEquals(trainDeparture1, sortedTrainDepartures.get(1));
    assertEquals(trainDeparture2, sortedTrainDepartures.get(2));
  }

  @Test
  @DisplayName("Positive test")
  void testRemoveExistingTrainDepartureByNumber() {
    // Arrange
    TrainDeparture trainDeparture1 = new TrainDeparture(LocalTime.of(10, 0), "L2", "1", "Oslo", 2);
    TrainDeparture trainDeparture2 = new TrainDeparture(LocalTime.of(11, 0), "L2", "2", "Oslo", 2);
    TrainDeparture trainDeparture3 = new TrainDeparture(LocalTime.of(9, 0), "L2", "3", "Oslo", 2);

    trainSchedule.addTrainDeparture(trainDeparture1);
    trainSchedule.addTrainDeparture(trainDeparture2);
    trainSchedule.addTrainDeparture(trainDeparture3);

    // Act
    trainSchedule.removeExistingTrainDepartureByNumber("2");

    // Assert
    assertNull(trainSchedule.findTrainDepartureByNumber("2"));
    assertNotNull(trainSchedule.findTrainDepartureByNumber("1"));
    assertNotNull(trainSchedule.findTrainDepartureByNumber("3"));
  }

  @Test
  @DisplayName("Positve test")
  void testRemoveTrainDepaturesEarlierThanTime() {
    // Arrange
    TrainDeparture trainDeparture1 = new TrainDeparture(LocalTime.of(9, 0), "L2", "1", "Oslo", 2);
    TrainDeparture trainDeparture2 = new TrainDeparture(LocalTime.of(11, 0), "L2", "2", "Oslo", 2);
    TrainDeparture trainDeparture3 = new TrainDeparture(LocalTime.of(8, 0), "L2", "3", "Oslo", 2);

    trainSchedule.addTrainDeparture(trainDeparture1);
    trainSchedule.addTrainDeparture(trainDeparture2);
    trainSchedule.addTrainDeparture(trainDeparture3);

    // Act
    trainSchedule.removeTrainDepaturesEarlierThanTime();

    // Assert
    assertNull(trainSchedule.findTrainDepartureByNumber("1"));
    assertNull(trainSchedule.findTrainDepartureByNumber("3"));
    assertNotNull(trainSchedule.findTrainDepartureByNumber("2"));
  }
}

